<chapter id="inside">
  <title>Distributions inside Debian</title>

  <sect1 id="fork">
  <title>To fork or not to fork</title>

<para>
There are many distributions that decided to fork from a certain
state of Debian.  This is perfectly all right because Debian is
completely free and everybody is allowed to do this.  People who
built those derived distributions had certain reasons to proceed
this way.
</para>

  <sect2 id="commercialfork">
  <title>Commercial forks</title>

<para>
If Debian should be used as the base for a commercial distribution
like 
  <ulink url="http://www.linspire.com/">Linspire</ulink> (formerly Lindows),
  <ulink url="http://www.libranet.com/">Libranet</ulink> or
  <ulink url="http://www.xandros.com/">Xandros</ulink>, there is no other
choice than forking because these companies normally add some stuff
that is non-free.  While Debian Pure Blends might be interesting in
technical terms for those commercial distributions by making it easier
to build a separate distribution, these non-free additions are not
allowed to be integrated into Debian, and thus integration into Debian
is impossible.
</para>
  </sect2>

  <sect2 id="noncommercialfork">
  <title>Non-commercial forks</title>

<para>
As a completely free distribution Debian GNU/Linux is quite often a
welcome starting point for derived distributions with a certain
purpose that are as free as Debian but had certain reasons to fork.
One main reason for a fork was that Debian was not flexible enough for
certain purposes and some needed features had to be added.  One reason
for the Debian Pure Blends effort is to increase flexibility and to
make the reason mentioned above void (if it is not yet void because of
the general develoment of Debian).  Some examples of forks from Debian
that are probably now able to integrate back into Debian as a Debian
Pure Blend are:

<variablelist>

<varlistentry>
 <term><ulink url="http://www.skolelinux.org">SkoleLinux</ulink></term>
  <listitem><para>Mentioning SkoleLinux in the category of forks is more or less
        history.  The merge back into Debian started with the
        SkoleLinux people really doing a great job to enhance Debian
        for their own purposes in the form of their work on
        debian-installer, and culminated with the formal merging of
        the Blend Debian Edu and SkoleLinux, so that they are now
        virtually equivalent.  This is the recommended way for derived
        distributions, and the reasons for this recommendation are
        given below.</para>
  </listitem>
</varlistentry>

<varlistentry>
 <term>DeMuDi</term>
  <listitem><para>The <ulink url="http://www.agnula.org/">Agnula</ulink> project, which is founded by the
	European Community, (and in fact is the first Free Software project
	that was founded by the EU at all,) forked for the
	following reasons:</para>

      <variablelist>

      <varlistentry>
        <term>Technical</term>
      	 <listitem><para>They had some special requirements for the kernel and
    	 configuration. This is more or less solved in the
    	 upcoming Debian release.</para></listitem>
      </varlistentry>

      <varlistentry>
        <term>License</term>
           <listitem><para>When DeMuDi started, not enough free programs in this
  	 field existed.  This situation is better now.</para></listitem>
    </varlistentry>

    <varlistentry>
        <term>Organisational</term>
           <listitem><para>Because of the founded status of the project, an extra
  	 distribution had to be developed.  To accomplish this
  	 requirement, Debian Pure Blends plan to build common tools to
  	 facilitate building separate CDs with the contents of only a
  	 single distribution.</para></listitem>
    </varlistentry>

       </variablelist>

      <para>
        This shows that there is no longer a real need for a fork, and
	in fact, the organiser of the DeMuDi project was in contact to
	start bringing DeMuDi back into Debian.  That is why DeMuDi is
	mentioned in the list of Debian Pure Blends above.
	Unfortunately the effort to merge back has stalled but it
	might be an interesting project to apply Blends techniques to
	support multimedia experts who want to use Debian.</para>
  </listitem>
</varlistentry>
 
<varlistentry>
 <term>LinEx</term>
  <listitem><para>LinEx is the very successful distribution for schools in the
        Region Extremadura in Spain.  The work of the LinEx people
        perhaps made Debian more popular than any other distribution.
        The project was founded by the local government of
        Extremadura, and each school in this region is running this
        distribution.  While this is a great success, the further
        development of LinEx has to face the problems that will be
        explained below.  Because the creators of LinEx are aware of
        this fact they started joining the educational part of LinEx
        with Debian Edu which in turn leaded to an even stronger
        position of this Blend.</para>
  </listitem>
</varlistentry>

</variablelist>

</para>

<para>
If developers of a non-commercial fork consider integrating back into
Debian in the form of a Debian Pure Blend, it might happen that their
field is covered already by an existing Blend.  For instance, this
would be the case for LinEx, which has the same group of target users
as Debian Edu as explained above.  On the other hand, some special
adaptations might be necessary to fit the requirements of the local
educational system.  The specific changes that might be necessary
would be called <emphasis>flavours</emphasis> of a Blend.
</para>
  </sect2>

  <sect2 id="disadvantages">
  <title>Disadvantages of separate distribution</title>

<para>
In general, a separate distribution costs extra effort.  Because it is
hardly possible to hire enough developers who can double the great
work of many volunteer Debian developers, this would be a bad idea for
economical reasons.  These people would have to deal with continuous
changes to keep the base system, installer, etc. up to date with the
current Debian development.  It would be more sane to send patches that
address their special requirements to Debian instead of
maintaining a complete Debian tree containing these patches.
</para>

<para>
Debian is well known for its strong focus on security.  Security is
mainly based on manpower and knowledge.  So the best way to deal with
security issues would be to base it on the Debian infrastructure,
instead of inventing something new.
</para>

<para>
New projects with special intentions often have trouble to become
popular to the user group they want to address.  This is a matter of
attaining the critical mass that was explained in <xref linkend="general_problem"/>.
</para>
<para>
Larger Free Software projects need certain infrastructure like
web servers, ftp servers, (both with mirrors,) a bug tracking system,
etc.  It takes a fair amount of extra effort to build an entire infrastructure
that is already available for free in Debian.
</para>
<para>
<emphasis>Forking would be a bad idea.</emphasis>
</para>
  </sect2>

  <sect2 id="advantages">
  <title>Advantages of integration into Debian</title>

<para>
Debian has a huge user base all over the world.  Any project that is
integrated within Debian has a good chance to become popular on the back
of Debian if the target users of the project just notice that it
enables them to solve their problems.  So there is no
need for extra research on the side of the users, and no need for
advertising for a special distribution.  This fact has been
observed in the Debian Med project, which is well known for many
people in medical care.  It would not have gained this popularity if
it had been separated from Debian.
</para>

<para>
You get a secure and stable system without extra effort for free.
</para>

<para>
Debian offers a sophisticated Bug Tracking System for free, which is a
really important resource for development.
</para>

<para>
There is a solid infrastructure of web servers, ftp servers with
mirrors, mail servers, and an LDAP directory of developers with a strongly
woven web of trust (through gpg key signing) for free.
</para>

</sect2>

   <sect2>
   <title>Enhancing Debian</title>

<para>
By making changes to some packages to make them fit the needs of a target
user group, the overall quality of Debian can be enhanced.  In this way,
enhancing Debian by making it more user friendly is a good way for
the community to give back something to Debian.  It would be a shame
if somebody would refuse all the advantages to keeping a project
inside Debian, and instead would decide to try to cope with the disadvantages
because he just does not know how to do it the right way, and that it is
normally easy to propogate changes into Debian. For instance, see 
<xref linkend="howto_itp"/>.  This section explains how you can ask for 
a certain piece of software to be included in Debian.
The next section describes the reason why Debian is flexible enough
to be adapted to any purpose.
</para>

</sect2>

</sect1>

  <sect1>
  <title>Adaptation to any purpose</title>

<para>
Debian is developed by about 1000 volunteers.  Within this large group,
the developers are encouraged to care for their own interests in packages
they have chosen to look after.  Thus, Debian is not bound to commercial
interests.
</para>

<para>
Those who might fear this amount of freedom given to every single developer
should realize that there are very strict rules, as laid out in 
<ulink url="http://www.debian.org/doc/debian-policy/">Debian's policy</ulink>,
which glue everything together.  To keep their packages in each new
release, every developer must ensure that their packages abide by that policy.
</para>

<para>
One common interest each individual developer shares is to make the
best operating system for himself.  This way, people with similar
interests and tasks profit from the work of single developers.  If
users, in turn, work together with the developers by sending patches or
bug reports for further enhancement, Debian can be improved also for
special tasks.
</para>

<para>
For instance, developers may have children, or may work in some
special fields of work, and so they try to make the best system for
their own needs.  For children, they contribute to Debian Jr. or
Debian Edu.  For their field of work, they contribute to the
appropriate Blend: Debian Med, Debian Science, and so forth.
</para>

<para>
In contrast to employees of companies, every single Debian developer
has the freedom and ability to realize his vision.  He is not bound to
decisions of the management of his company.  Commercial distributors
have to aim their distributions at gaining a big market share.  The
commercial possibilities in targeting children's PCs at home are
slight, so distributions comparable to Debian Junior are not attractive
for commercial distributors to make.
</para>

<para>
Thus, single developers have influence on development - they just have
to <emphasis>do</emphasis> it, which is a very different position compared
with employees of a commercial distributor.  This is the reason for the
flexibility of Debian that makes it adaptable for any purpose.  In
the Debian world, this kind of community is called
"<emphasis><emphasis>Do</emphasis>ocracy</emphasis>" - the one who does, rules.
</para> 
  </sect1>

</chapter>